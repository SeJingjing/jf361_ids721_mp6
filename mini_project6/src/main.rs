use log::{info, error};
use env_logger;
use lambda_runtime::{LambdaEvent, Error as LambdaError, service_fn};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use aws_config::load_from_env;
use aws_sdk_dynamodb::{Client, model::AttributeValue};


#[derive(Deserialize, Debug)]
struct Request {
    course_id: Option<String>,
}


#[derive(Serialize)]
struct Response {
    course_name: Option<String>,
    semester: Option<String>,
}


#[tokio::main]
async fn main() -> Result<(), LambdaError> {
    env_logger::init();
    info!("Starting the Lambda function");
    let func = service_fn(handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn handler(event: LambdaEvent<Value>) -> Result<Value, LambdaError> {
    info!("Received event: {:?}", event);
    let request: Request = serde_json::from_value(event.payload)?;
    info!("Parsed request: {:?}", request);

    let config = load_from_env().await;
    let client = Client::new(&config);

    let response = search_course_info(&client, request.course_id).await?;

    Ok(json!(response))
}

async fn search_course_info(client: &Client, course_id: Option<String>) -> Result<Response, LambdaError> {
    info!("Searching course info for course_id: {:?}", course_id);

    let table_name = "course_info";

    if let Some(course_id_val) = course_id {
        let result = client.get_item()
            .table_name(table_name)
            .key("course_id", AttributeValue::S(course_id_val))
            .send()
            .await?;

        if let Some(item) = result.item {
            let course_name = item.get("course_name").and_then(|val| val.as_s().ok()).map(|s| s.to_string());
            let semester = item.get("semester").and_then(|val| val.as_s().ok()).map(|s| s.to_string());

            info!("Found course info: {:?}", item);
            Ok(Response {
                course_name,
                semester,
            })
        } else {
            error!("Error: No matching course found");
            Err(LambdaError::from("No matching course found"))
        }
    } else {
        error!("Error: course_id is required");
        Err(LambdaError::from("course_id is required"))
    }
}
