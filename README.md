# jf361_ids721_mp6

## Project Introduction
This project involves the development of a Rust AWS Lambda function with logging features, incorporation of AWS X-Ray tracing for in-depth analysis of the function's performance, and consolidation of all logs and traces in AWS CloudWatch for monitoring and evaluation.

## Project Description
- Add logging to a Rust Lambda function
- Integrate AWS X-Ray tracing
- Connect logs/traces to CloudWatch

## Project Setup
   #### 1. create a simple AWS RUST Lambda function
1. Use below code to create a sample RUST funciton.
    ``` 
        cargo lambda new <PROJECT-NAME>
        cd <PROJECT-NAME>
    ```

2. Implement the function in `src/main.rs` and add dependencies in `Cargo.toml`.

#### 2.  Deploy project (Since I used almost the same function from mini project 5, the deployment process is the same with MP5.)
1. Deploy the RUST function project to AWS.
    ```
        cargo lambda build --release
        cargo lambda deploy --region <AWS-REGION> --iam-role <ROLE-IAM>
    ```

2. Search for `Lambda` on the AWS console and find the lambda function with name <PROJECT-NAME>. 
3. In this function, add a trigger:`API Gateway` with type of `REST API` to it.
4. Click the created API Gateway, create a new resource for it.
5. In this new resource, create a  `ANY` method and then `enable CORS`
6. Click `Deploy API` and create a new stage.

#### 3. Set up the X-ray monitoring for lambda function.

In the `Lambda` console, under `configuration` section, there is an option: `Monitoring and operations tools`. Select `Edit` in `Addtional monitoring tools` and enable `X-Ray active tracing`.
   <p align="center">
       <img src="monitortool.png" />
   </p>

#### 4. Test the functionality
Use the URL to test functionality (Successful case)
   <p align="center">
       <img src="postman.png" />
   </p>

   and use `Test` in the `Lambda` console to test the error cases.
   
   ##### Error Cases 1 
   Use a test(no `course_id`) to check the function
   <p align="center">
       <img src="testcode1.png" />
   </p>
   The log output
   <p align="center">
       <img src="testresult1.png" />
   </p>

   ##### Error Cases 2
   Use a test(wrong `course_id`) to check the function
   <p align="center">
       <img src="testcode2.png" />
   </p>
   The log output
   <p align="center">
       <img src="testresult2.png" />
   </p>

#### 5. CloudWatch
Search for `CloudWatch` on the AWS console. Click `Traces` under `X-Ray traces`. Then you can see the logs.
   <p align="center">
       <img src="CloudWatch.png" />
   </p>

